import React, { Component } from "react";

class Outros extends Component {
  constructor(props) {
    super(props);
    this.state = {
      others: []
    }
  };
  componentDidMount() {
    let url = "http://localhost:3001/others"
    fetch(url)
      .then(resp => resp.json())
      .then(data => {
        let others = data.map((others, index) => {
          return (
            <div key="{index}">
              <p>{others.text}</p>
            </div>
          )
        })
        this.setState({ others: others })
      })
  }
  render() {
    return (
      <div className="app">
        {this.state.others}
      </div>
    )
  }
}

export default Outros;
