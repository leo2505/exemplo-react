import React, { Component } from "react";

class Stuff extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stufs: []
    }
  };
  componentDidMount() {
    let url = "http://localhost:3001/stufs"
    fetch(url)
      .then(resp => resp.json())
      .then(data => {
        let posts = data.map((stufs, index) => {
          return (
            <div key="{index}">
              <h2>{stufs.title}</h2>
              <p></p>
              <ol>
                <li>{stufs.li1}</li>
                <p>{stufs.p}</p>
                <li>{stufs.li2}</li>
                <p>{stufs.p2}</p>
              </ol>
            </div>
          )
        })
        this.setState({ posts: posts })
      })
  }
  render() {
    return (
      <div className="app">
        {this.state.posts}
      </div>
    )
  }
}

export default Stuff;
