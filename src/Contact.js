import React, { Component } from "react";

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      native: []
    }
  };
  componentDidMount() {
    let url = "http://localhost:3001/native"
    fetch(url)
      .then(resp => resp.json())
      .then(data => {
        let posts = data.map((posts, index) => {
          return (
            <div key="{index}">
              <h2>{posts.title}</h2>
              <p>{posts.text}</p>
            </div>
          )
        })
        this.setState({ posts: posts })
      })
  }
  render() {
    return (
      <div className="app">
        {this.state.posts}
      </div>
    )
  }
}

export default Contact;
