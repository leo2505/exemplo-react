import React, { Component } from "react";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      home: []
    }
  };
  componentDidMount() {
    let url = "http://localhost:3001/home"
    fetch(url)
      .then(resp => resp.json())
      .then(data => {
        let posts = data.map((posts, index) => {
          return (
            <div key="{index}">
              <h2>{posts.title}</h2>
              <p>{posts.text}</p>
              <p>{posts.textt}</p>
              <p>{posts.text2}</p>
              <p><img src={posts.logo} /></p>
            </div>
          )
        })
        this.setState({ posts: posts })
      })
  }
  render() {
    return (
      <div className="app">
        {this.state.posts}
      </div>
    )
  }
}

export default Home;
