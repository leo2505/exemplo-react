import React, { Component } from "react";
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Home from "./Home";
import Stuff from "./Stuff";
import Contact from "./Contact";
import Outros from "./Outros";



class Main extends Component {
  render() {
    return (
      <HashRouter>
        <div>
          <h1>REACT</h1>
          <ul className="header">
            <li><NavLink exact to="/">Home</NavLink></li>
            <li><NavLink to="/fake">Fake Server</NavLink></li>
            <li><NavLink to="/native">Native</NavLink></li>
            <li><NavLink to="/outros">Outros</NavLink></li>
          </ul>
          <div className="content">
            <Route exact path="/" component={Home} />
            <Route path="/fake" component={Stuff} />
            <Route path="/native" component={Contact} />
            
            <Route path="/outros" component={Outros} />
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default Main;
