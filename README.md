This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm run start`
Rodar o front-end (roda na 3000)

### `json-server --watch --port 3001 db.json`
Rodar o fake server
